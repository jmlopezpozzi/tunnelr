/* tunnelr.c */
/* a demoscene-like raytraced tunnel demo */

#include <raylib.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>


// Program settings
#define DEFAULT_FILENAME "default.png"
#define SCR_WIDTH 640
#define SCR_HEIGHT 400
#define GRID_RATIO 2
#define GRID_WIDTH (SCR_WIDTH / GRID_RATIO)
#define GRID_HEIGHT (SCR_HEIGHT / GRID_RATIO)
#define INTERPOLATION_STEP 3
#define INTERPOLATION_Y_STEP INTERPOLATION_STEP
#define INTERPOLATION_X_STEP INTERPOLATION_STEP
#define RADIUS_RATIO 0.5f  // Used for defining the initial radius of the tunnel (which will be texture_width/RADIUS_RATIO)
#define BG_COLOR BLACK


// Keys for controlling the position and direction vectors z
#define KEY_VPZ_UP KEY_Q
#define KEY_VPZ_DOWN KEY_A
#define KEY_VDZ_UP KEY_W
#define KEY_VDZ_DOWN KEY_S

// Keys for controlling the tunnel radius and texture mapping ratios
#define KEY_RADIUS_UP KEY_E
#define KEY_RADIUS_DOWN KEY_D
#define KEY_UR_UP KEY_R
#define KEY_UR_DOWN KEY_F
#define KEY_VR_UP KEY_T
#define KEY_VR_DOWN KEY_G

#define PD_DELTA 1.0f  // vpz and vdz variation step
#define U_DELTA 0.25f  // ur variation step

// Movement keys
#define KEY_ACC KEY_UP
#define KEY_DEC KEY_DOWN
#define KEY_MOVE_RIGHT KEY_RIGHT
#define KEY_MOVE_LEFT KEY_LEFT
#define KEY_STOP_MOVE KEY_M

#define SPD_MAX 0.01f     // Maximum speed when going forwards
#define SPD_MIN -SPD_MAX  // Maximum speed when going backwards
#define ACC 0.0005f       // Speed variation step

// Keys for controlling the position vector in (x, y)
#define KEY_VPY_UP KEY_KP_8
#define KEY_VPY_DOWN KEY_KP_2
#define KEY_VPX_LEFT KEY_KP_4
#define KEY_VPX_RIGHT KEY_KP_6
#define KEY_VP_CENTER KEY_KP_5

#define RP_DELTA 5.0f  // vpy and vpx variation step

// Keys for controlling the direction vectors offset in (x, y)
#define KEY_VDY_UP KEY_Y
#define KEY_VDY_DOWN KEY_H
#define KEY_VDX_LEFT KEY_U
#define KEY_VDX_RIGHT KEY_I
#define KEY_CENTERLOOK KEY_J

#define RD_DELTA 1.0f  // vdy and vdx variation step

// Keys for controlling the rotation of the direction vectors in the z axis
#define KEY_ROTATE_LEFT KEY_KP_9
#define KEY_ROTATE_RIGHT KEY_KP_7
#define KEY_ROTATOR_RESET KEY_KP_1

#define ROTATOR_DELTA 0.03f  // Direction vectors rotation variation step


#define KEY_CHANGE_MOVEMENT KEY_ZERO  // Toggles between the "movement free" and "movement friction" modes
#define KEY_CAM_ANGLE_FIX KEY_NINE    // Toggles the "fixed angle camera" on and off
#define KEY_FPS KEY_K                 // Toggles between locked and unlocked FPS
#define KEY_SHOWINFO KEY_P            // Toggles tunnel parameter information display

// Keys for controlling the rendering interpolation values during runtime
#define KEY_I_DOWN KEY_ONE
#define KEY_I_UP KEY_TWO
#define KEY_I_Y_DOWN KEY_THREE
#define KEY_I_Y_UP KEY_FOUR
#define KEY_I_X_DOWN KEY_FIVE
#define KEY_I_X_UP KEY_SIX
#define KEY_I_Y_TO_X KEY_SEVEN
#define KEY_I_X_TO_Y KEY_EIGHT


// Texture coordinate type
typedef struct UV_Sample {
	unsigned u;
	unsigned v;
} UV_Sample;

enum mov_state {STILL, ACCELERATING, TOP_SPEED, DECELERATING};  // States for the "movement friction" movement type


float vpz = -1.0f;  // Origin z
float vpy = 0.0f;   // Origin y
float vpx = 0.0f;   // Origin x
float vdz = 1.0f;   // Direction z
float vdy = 0.0f;   // Direction y
float vdx = 0.0f;   // Direction x
float ur = 8.0f;    // Texture height ratio
float vr;           // Texture width ratio
float radius;       // Tunnel radius

bool cam_angle_fix_g = false;  // Sets the "fixed camera angle" mode
bool fpsunlock = false;        // Unlocks the FPS target
bool showinfo = true;          // Displays tunnel parameter values


int tunnelr(Color *tex_data, int tex_width, int tex_height);
void compute_direction_vectors(Vector3 directions[], int grid_width,
                               int grid_height, int y_step, int x_step);
void rotate_direction_vectors(Vector3 directions[], float angle, int grid_width,
                              int grid_height, int y_step, int x_step);
void compute_grid_intersections(Vector3 directions[], UV_Sample uv_samples[],
                                int grid_width, int grid_height, int y_step,
                                int x_step, int tex_pos_u, int tex_pos_v,
                                int tex_width);
void render_interpolated(Color buffer[], Color *tex_data,
                         UV_Sample uv_samples[], int grid_width,
                         int grid_height, int y_step, int x_step, int tex_width,
                         int tex_height);
float lerp(float s, float e, float t);
float blerp(float c00, float c10, float c01, float c11, float tx, float ty);
void control(float *tex_pos_u, float *tex_pos_v, float *spd_u, float *spd_v,
             int *i_y_step, int *i_x_step, Vector2 *fb_offset, float *r_angle,
             Color *buffer, int tex_width);
void movement_free(float *spd_u, float *spd_v);
void movement_friction(float *spd_u, float *spd_v);
enum mov_state compute_movement_friction(float *spd_ptr, int key_posi,
                                         int key_nega, enum mov_state state);


int main(int argc, char *argv[])
{
	if (argc > 2) {
		fprintf(stderr, "tunnelr, a demoscene-like raytraced tunnel demo\n"
		                "usage: tunnelr [texture_filename]\n"
		                "if no texture_filename is provided, " DEFAULT_FILENAME
		                " will be used\n");
		return 1;
	}

	Image tex;
	Color *tex_data;

	InitWindow(SCR_WIDTH, SCR_HEIGHT, "raytraced tunnel");
	SetTargetFPS(60);

	if (argc == 2) {
		tex = LoadImage(argv[1]);
	}
	else {
		tex = LoadImage(DEFAULT_FILENAME);
	}

	if (tex.data == NULL) {
		CloseWindow();

		return 1;
	}

	if (tex.width < 1 || tex.width > INT_MAX / 2 ||
	    tex. height < 1 || tex.height > INT_MAX / 2)
	{
		TraceLog(LOG_INFO, "Invalid image, shutting down");
		UnloadImage(tex);
		CloseWindow();

		return 1;
	}

	tex_data = GetImageData(tex);
	if (tex_data == NULL) {
		UnloadImage(tex);
		CloseWindow();

		return 1;
	}

	tunnelr(tex_data, tex.width, tex.height);

	free(tex_data);
	UnloadImage(tex);
	CloseWindow();

	return 0;
}

int tunnelr(Color *tex_data, int tex_width, int tex_height)
{
	Vector3 directions[GRID_HEIGHT * GRID_WIDTH];
	UV_Sample uv_samples[GRID_HEIGHT * GRID_WIDTH];
	Color buffer[GRID_HEIGHT * GRID_WIDTH];
	RenderTexture2D fb_tex;

	int i_y_step = INTERPOLATION_Y_STEP;
	int i_x_step = INTERPOLATION_X_STEP;
	Vector2 fb_offset = {.x = (INTERPOLATION_X_STEP / 2) * GRID_RATIO + 1,
	                     .y = (INTERPOLATION_Y_STEP / 2) * GRID_RATIO + 1};  // Tries to centers the framebuffer texture

	// We move through the tunnel by shfting the texture across its height
	// We rotate the tunnel by shifting the texture across its width
	// Here the range for the texture position variables is 0.0f - 1.0f
	float tex_pos_u = 0.0f;
	float tex_pos_v = 0.0f;
	float spd_u = 0.0f;
	float spd_v = 0.0f;

	float rotation_angle = 0.0f;  // Viewport rotation angle (accomplished by rotating the direction vectors)

	fb_tex = LoadRenderTexture(GRID_WIDTH, GRID_HEIGHT);
	SetTextureFilter(fb_tex.texture, FILTER_POINT);

	vr = tex_width;
	radius = tex_width / RADIUS_RATIO;

	while (!WindowShouldClose()) {
		control(&tex_pos_u, &tex_pos_v, &spd_u, &spd_v, &i_y_step, &i_x_step,
		        &fb_offset, &rotation_angle, buffer, tex_width);

		compute_direction_vectors(directions, GRID_WIDTH, GRID_HEIGHT,
		                          i_y_step, i_x_step);

		rotate_direction_vectors(directions, rotation_angle, GRID_WIDTH,
		                         GRID_HEIGHT, i_y_step, i_x_step);

		compute_grid_intersections(directions, uv_samples, GRID_WIDTH,
		                           GRID_HEIGHT, i_y_step, i_x_step,
		                           tex_pos_u * tex_height,
		                           tex_pos_v * tex_width, tex_width);  // Texture position values are multiplied by actual texture size to get absolute position

		render_interpolated(buffer, tex_data, uv_samples, GRID_WIDTH,
		                    GRID_HEIGHT, i_y_step, i_x_step, tex_width,
		                    tex_height);

		BeginDrawing();

		ClearBackground(BG_COLOR);
		UpdateTexture(fb_tex.texture, buffer);
		DrawTextureEx(fb_tex.texture, fb_offset, 0.0f, GRID_RATIO, RAYWHITE);

		if (showinfo == true) {
			DrawText(TextFormat("\nfps=%d\nvpz=%f\nvdz=%f\nradius=%f\nur=%f\n"
			                    "vr=%f\nspd_u=%f\ntex_pos_u=%f\nspd_v=%f\n"
			                    "tex_pos_v=%f\nvpx=%f\nvpy=%f\nvdx=%f\nvdy=%f\n"
			                    "r_angle=%f\ny_step=%d\nx_step=%d",
			                    GetFPS(), vpz, vdz, radius, ur, vr, spd_u,
			                    tex_pos_u, spd_v, tex_pos_v, vpx, vpy, vdx, vdy,
			                    rotation_angle, i_y_step, i_x_step),
			         0, 0, 10, RAYWHITE);
		}

		EndDrawing();
	}

	UnloadRenderTexture(fb_tex);
	return 0;
}

void compute_direction_vectors(Vector3 directions[], int grid_width,
                               int grid_height, int y_step, int x_step)
{
	Vector3 direction;
	float direction_z = vdz;

	for (int y = 0; y < grid_height; y += y_step) {
		for (int x = 0; x < grid_width; x += x_step) {

			direction.x = x + vdx - (grid_width / 2);
			direction.y = y + vdy - (grid_height / 2);
			direction.z = direction_z;

			// Normalize the direction vector
			{
				float len = sqrtf(direction.x * direction.x +
				                  direction.y * direction.y +
				                  direction.z * direction.z);
				direction.x /= len;
				direction.y /= len;
				direction.z /= len;
			}

			directions[y * grid_width + x] = direction;			
		}
	}
}

void rotate_direction_vectors(Vector3 directions[], float angle, int grid_width,
                              int grid_height, int y_step, int x_step)
{
	float saved_component;

	for (int y = 0; y < grid_height; y += y_step) {
		for (int x = 0; x < grid_width; x += x_step) {

			Vector3 *cur_vec = &directions[y * grid_width + x];

			// Z axis
			saved_component = cur_vec->x;
			cur_vec->x = cur_vec->x * cosf(angle) - cur_vec->y * sinf(angle);
			cur_vec->y = saved_component * sinf(angle) + cur_vec->y * cosf(angle);
		}
	}
}

/* Note: here, tex_pos_u and tex_pos_v use the range 0 to tex_height and 0 to
   tex_width respectively */
void compute_grid_intersections(Vector3 directions[], UV_Sample uv_samples[],
                                int grid_width, int grid_height, int y_step,
                                int x_step, int tex_pos_u, int tex_pos_v,
                                int tex_width)
{
	Vector3 position = {.x = vpx, .y = vpy, .z = vpz};
	float t;  // Ray intersection point

	if (cam_angle_fix_g == true) {
		float angle = (tex_pos_v / (float) tex_width) * 2.0f * PI;

		position.x = (vpx * cosf(angle) - vpy * sinf(angle));
		position.y = (vpx * sinf(angle) + vpy * cosf(angle));

		tex_pos_v = 0;
	}

	for (int y = 0; y < grid_height; y += y_step) {
		for (int x = 0; x < grid_width; x += x_step) {

			Vector3 direction = directions[y * grid_width + x];

			float a = direction.x * direction.x + direction.y * direction.y;
			float b = 2 * (position.x * direction.x +
			               position.y * direction.y);
			float c = position.x * position.x + position.y * position.y -
			          radius * radius;

			float delta = powf(b, 2) - 4 * a * c;

			if (delta < 0.0f) {
				continue;  // No intersection
			}
			else
			if (delta == 0.0f) {
				t = -b / 2 * a;  // Only one solution
			}
			else {
				float delta_sqrt = sqrtf(delta);
				float t1 = (-b - delta_sqrt) / 2 * a;
				float t2 = (-b + delta_sqrt) / 2 * a;

				t = fminf(t1, t2);  // Two solutions, we keep the nearest
			}

			Vector3 intersection;

			intersection.x = position.x + t * direction.x;
			intersection.y = position.y + t * direction.y;
			intersection.z = position.z + t * direction.z;

			// Cylindric mapping
			UV_Sample uv_sample;

			uv_sample.u = (unsigned) (fabsf(intersection.z) * ur) + tex_pos_u;
			uv_sample.v = (unsigned) (0.5f * (atan2f(intersection.y, intersection.x) + 2 * PI) * vr / PI) + tex_pos_v;   // Adding 2*PI maps the result of atan2 to a positive range

			uv_samples[y * grid_width + x] = uv_sample;
		}
	}
}

void render_interpolated(Color buffer[], Color *tex_data,
                         UV_Sample uv_samples[], int grid_width,
                         int grid_height, int y_step, int x_step, int tex_width,
                         int tex_height)
{
	for (int gy = y_step; gy < grid_height; gy += y_step) {
		for (int gx = x_step; gx < grid_width; gx += x_step) {

			int x0 = gx - x_step;
			int x1 = gx;
			int y0 = gy - y_step;
			int y1 = gy;

			UV_Sample uv00 = uv_samples[y0 * grid_width + x0];
			UV_Sample uv01 = uv_samples[y0 * grid_width + x1];
			UV_Sample uv10 = uv_samples[y1 * grid_width + x0];
			UV_Sample uv11 = uv_samples[y1 * grid_width + x1];

			// These ifs avoid the wrap-around v visual glitch. All branches can be taken depending on the origin vector
			if (abs(uv01.v - uv00.v) > vr / 2) {
				if (uv01.v < uv00.v) {
					uv01.v += vr;
				}
				else {
					uv00.v += vr;
				}
			}
			if (abs(uv11.v - uv10.v) > vr / 2) {
				if (uv11.v < uv10.v) {
					uv11.v += vr;
				}
				else {
					uv10.v += vr;
				}
			}

			// These ifs are also needed to avoid the glitch. The commented branches are only needed when we rotate the direction vectors.
			if (abs(uv01.v - uv11.v) > vr / 2) {
				if (uv11.v < uv01.v) {
					uv11.v += vr;
				}
				else {
					uv01.v += vr;  // Not needed if the direction vectors do not rotate
				}
			}
			if (abs(uv00.v - uv10.v) > vr / 2) {
				if (uv10.v < uv00.v) {
					uv10.v += vr;
				}
				else {
					uv00.v += vr;  // Not needed if the direction vectors do not rotate
				}
			}

			for (int j = 0; j < y_step; ++j) {
				for (int i = 0; i < x_step; ++i) {
					float tx = i / (float) x_step;
					float ty = j / (float) y_step;

					uv_samples[(y0 + j) * grid_width + x0 + i].u = (unsigned) blerp(uv00.u, uv01.u, uv10.u, uv11.u, tx, ty) % tex_height;
					uv_samples[(y0 + j) * grid_width + x0 + i].v = (unsigned) blerp(uv00.v, uv01.v, uv10.v, uv11.v, tx, ty) % tex_width;
				}
			}
		}
	}

	for (int y = 0; y < grid_height - y_step; ++y) {
		for (int x = 0; x < grid_width - x_step; ++x) {

			int index = y * grid_width + x;

			buffer[index] = tex_data[uv_samples[index].u * tex_width + uv_samples[index].v];
		}
	}
}

/* Linear interpolation function */
float lerp(float s, float e, float t)
{
	return s + (e - s) * t;
}

/* Bilinear interpolation function */
float blerp(float c00, float c10, float c01, float c11, float tx, float ty)
{
    return lerp(lerp(c00, c10, tx), lerp(c01, c11, tx), ty);
}

void control(float *tex_pos_u, float *tex_pos_v, float *spd_u, float *spd_v,
             int *i_y_step, int *i_x_step, Vector2 *fb_offset,float *r_angle,
             Color *buffer, int tex_width)
{
	static enum {MOVE_FREE = 0, MOVE_FRIC} movement_type = MOVE_FREE;    // Current movement type
	static void (*movement_function)(float *, float *) = movement_free;  // Current movement computation function

	// Change position vector z
	if (IsKeyDown(KEY_VPZ_UP)) {
		vpz += PD_DELTA;
	}
	if (IsKeyDown(KEY_VPZ_DOWN)) {
		vpz -= PD_DELTA;
	}

	// Change direction vector z
	if (IsKeyPressed(KEY_VDZ_UP)) {
		vdz += PD_DELTA;
	}
	if (IsKeyPressed(KEY_VDZ_DOWN)) {
		vdz -= PD_DELTA;
	}

	// Change u ratio
	if (IsKeyDown(KEY_UR_UP)) {
		ur += U_DELTA;
	}
	if (IsKeyDown(KEY_UR_DOWN)) {
		ur -= U_DELTA;
	}

	// Change v ratio
	if (IsKeyPressed(KEY_VR_UP)) {
		vr += tex_width;
	}
	if (IsKeyPressed(KEY_VR_DOWN)) {
		vr -= tex_width;
	}

	// Change radius
	if (IsKeyDown(KEY_RADIUS_UP)) {
		radius += PD_DELTA;
	}
	if (IsKeyDown(KEY_RADIUS_DOWN)) {
		radius -= PD_DELTA;
	}

	// Camera (moves the origin vector in the (x, y) plane)
	if (IsKeyDown(KEY_VPY_UP)) {
		vpy += RP_DELTA;
	}
	if (IsKeyDown(KEY_VPY_DOWN)) {
		vpy -= RP_DELTA;
	}
	if (IsKeyDown(KEY_VPX_LEFT)) {
		vpx += RP_DELTA;
	}
	if (IsKeyDown(KEY_VPX_RIGHT)) {
		vpx -= RP_DELTA;
	}
	if (IsKeyPressed(KEY_VP_CENTER)) {
		vpx = 0.0f;
		vpy = 0.0f;
	}

	// Look around (changes the direction vector in the (x, y) plane)
	if (IsKeyDown(KEY_VDX_RIGHT)) {
		vdx -= RD_DELTA;
	}
	if (IsKeyDown(KEY_VDX_LEFT)) {
		vdx += RD_DELTA;
	}
	if (IsKeyDown(KEY_VDY_DOWN)) {
		vdy -= RD_DELTA;
	}
	if (IsKeyDown(KEY_VDY_UP)) {
		vdy += RD_DELTA;
	}
	if (IsKeyDown(KEY_CENTERLOOK)) {
		vdx = 0.0f;
		vdy = 0.0f;
	}

	// Change movement type
	if (IsKeyPressed(KEY_CHANGE_MOVEMENT)) {
		movement_type = 1 - movement_type;
		if (movement_type == MOVE_FREE) {
			movement_function = movement_free;
		}
		else {
			movement_function = movement_friction;
		}
	}

	// Movement
	(*movement_function)(spd_u, spd_v);

	*tex_pos_u += *spd_u;
	if (*tex_pos_u > 1.0f) {
		*tex_pos_u -= 1.0f;
	}
	if (*tex_pos_u < 0.0f) {
		*tex_pos_u += 1.0f;
	}
	*tex_pos_v += *spd_v;
	if (*tex_pos_v > 1.0f) {
		*tex_pos_v -= 1.0f;
	}
	if (*tex_pos_v < 0.0f) {
		*tex_pos_v += 1.0f;
	}

	// Toggle tunnel parameter values display
	if (IsKeyPressed(KEY_SHOWINFO)) {
		showinfo = 1 - showinfo;
	}

	// Toggle camera with fixed angle
	if (IsKeyPressed(KEY_CAM_ANGLE_FIX)) {
		cam_angle_fix_g = 1 - cam_angle_fix_g;
	}

	// Rotate direction vectors
	if (IsKeyDown(KEY_ROTATE_LEFT)) {
		*r_angle += ROTATOR_DELTA;
	}
	if (IsKeyDown(KEY_ROTATE_RIGHT)) {
		*r_angle -= ROTATOR_DELTA;
	}
	if (IsKeyPressed(KEY_ROTATOR_RESET)) {
		*r_angle = 0.0f;
	}

	// Unlock FPS
	if (IsKeyPressed(KEY_FPS)) {
		fpsunlock = 1 - fpsunlock;

		if (fpsunlock == true) {
			SetTargetFPS(0);
		}
		else {
			SetTargetFPS(60);
		}
	}

	// Change rendering interpolation
	{
		bool set_fb_offset = false;

		if (IsKeyPressed(KEY_I_DOWN)) {
			if (*i_y_step > *i_x_step) {
				*i_y_step = *i_x_step;
			}
			else {
				*i_x_step = *i_y_step;
			}

			if (*i_y_step > 1) {
				--*i_y_step;
				--*i_x_step;
			}
			set_fb_offset = true;
		}
		if (IsKeyPressed(KEY_I_UP)) {
			if (*i_y_step < *i_x_step) {
				*i_y_step = *i_x_step;
			}
			else {
				*i_x_step = *i_y_step;
			}

			if (*i_y_step < GRID_HEIGHT / 2) {  // Note: Assumes horizontal displays
				++*i_y_step;
				++*i_x_step;
			}
			set_fb_offset = true;
		}
		if (IsKeyPressed(KEY_I_Y_DOWN) && *i_y_step > 1) {
			--*i_y_step;
			set_fb_offset = true;
		}
		if (IsKeyPressed(KEY_I_Y_UP) && *i_y_step < GRID_HEIGHT / 2) {
			++*i_y_step;
			set_fb_offset = true;
		}
		if (IsKeyPressed(KEY_I_X_DOWN) && *i_x_step > 1) {
			--*i_x_step;
			set_fb_offset = true;
		}
		if (IsKeyPressed(KEY_I_X_UP) && *i_x_step < GRID_WIDTH / 2) {
			++*i_x_step;
			set_fb_offset = true;
		}
		if (IsKeyPressed(KEY_I_Y_TO_X)) {
			*i_y_step = *i_x_step;
			set_fb_offset = true;
		}
		if (IsKeyPressed(KEY_I_X_TO_Y)) {
			*i_x_step = *i_y_step;
			set_fb_offset = true;
		}
		if (set_fb_offset == true) {
			memset(buffer, 0, sizeof(Color) * GRID_WIDTH * GRID_HEIGHT);
			fb_offset->x = (*i_x_step / 2) * GRID_RATIO + 1;
			fb_offset->y = (*i_y_step / 2) * GRID_RATIO + 1;  // Tries to center the framebuffer texture
		}
	}
}

/* "Movement Free" movement type function */
void movement_free(float *spd_u, float *spd_v)
{
	if (IsKeyDown(KEY_ACC)) {
		*spd_u += ACC;
		if (*spd_u > SPD_MAX) {
			*spd_u = SPD_MAX;
		}
	}
	if (IsKeyDown(KEY_DEC)) {
		*spd_u -= ACC;
		if (*spd_u < SPD_MIN) {
			*spd_u = SPD_MIN;
		}
	}

	if (IsKeyDown(KEY_MOVE_LEFT)) {
		*spd_v += ACC;
		if (*spd_v > SPD_MAX) {
			*spd_v = SPD_MAX;
		}
	}
	if (IsKeyDown(KEY_MOVE_RIGHT)) {
		*spd_v -= ACC;
		if (*spd_v < SPD_MIN) {
			*spd_v = SPD_MIN;
		}
	}

	if (IsKeyDown(KEY_STOP_MOVE)) {
		*spd_u = 0.0f;
		*spd_v = 0.0f;
	}
}

/* "Movement Friction" movement type function */
void movement_friction(float *spd_u, float *spd_v)
{
	static enum mov_state state_u = STILL;
	static enum mov_state state_v = STILL;

	state_u = compute_movement_friction(spd_u, KEY_ACC, KEY_DEC, state_u);
	state_v = compute_movement_friction(spd_v, KEY_MOVE_LEFT, KEY_MOVE_RIGHT,
	                                    state_v);
}

enum mov_state compute_movement_friction(float *spd_ptr, int key_posi,
                                         int key_nega, enum mov_state state)
{
	switch (state) {
	case STILL:
		if (IsKeyDown(key_posi) || IsKeyDown(key_nega)) {
			state = DECELERATING;
		}
		break;

	case ACCELERATING:
		if (IsKeyDown(key_posi)) {
			*spd_ptr += ACC;
			if (*spd_ptr >= SPD_MAX) {
				*spd_ptr = SPD_MAX;
				state = TOP_SPEED;
			}
		}
		else
		if (IsKeyDown(key_nega)) {
			*spd_ptr -= ACC;
			if (*spd_ptr <= SPD_MIN) {
				*spd_ptr = SPD_MIN;
				state = TOP_SPEED;
			}
		}
		else {
			state = DECELERATING;
		}
		break;

	case TOP_SPEED:
		if ((*spd_ptr > 0.0f && !IsKeyDown(key_posi)) ||
		    (*spd_ptr < 0.0f && !IsKeyDown(key_nega))) {
			state = DECELERATING;
		}
		break;

	case DECELERATING:
		if (IsKeyDown(key_posi) || IsKeyDown(key_nega)) {
			state = ACCELERATING;
			break;
		}
		if (*spd_ptr > 0.0f) {
			*spd_ptr -= ACC / 2.0f;
			if (*spd_ptr <= 0.0f) {
				*spd_ptr = 0.0f;
				state = STILL;
			}
		}
		else
		if (*spd_ptr < 0.0f) {
			*spd_ptr += ACC / 2.0f;
			if (*spd_ptr >= 0.0f) {
				*spd_ptr = 0.0f;
				state = STILL;
			}
		}
		break;

	default:
		break;
	}

	if (IsKeyDown(KEY_STOP_MOVE)) {
		*spd_ptr = 0.0f;
		state = STILL;
	}

	return state;
}