/* xor.c: generates a red xor texture */

#include <raylib.h>

#define OUTPUT_FILENAME "xor.png"
#define TEX_SIDE 512

int main(void)
{
	Color texture[TEX_SIDE * TEX_SIDE];

	InitWindow(1, 1, "");

	for (int y = 0; y < TEX_SIDE; ++y) {
		for (int x = 0; x < TEX_SIDE; ++x) {
			texture[y * TEX_SIDE + x] = (Color){.r = (x * 256 / TEX_SIDE) ^ (y * 256 / TEX_SIDE), .g = 0, .b = 0, .a = 255};
		}
	}

	ExportImage(LoadImageEx(texture, TEX_SIDE, TEX_SIDE), OUTPUT_FILENAME);

	CloseWindow();

	return 0;
}